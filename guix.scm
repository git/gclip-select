;;;     Copyright 2022 Li-Cheng (Andy) Tai
;;;                      atai@atai.org

;;;     based on guix.scm of guile-gi,
;;;     Copyright (C) 2019 Jan Nieuwenhuizen <janneke@gnu.org>
;;;
;;; gclip_select is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-GI.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;
;; GNU Guix development package.  To build and play, run:
;;
;;   guix environment --ad-hoc -l guix.scm guile
;;
;; To build and install, run:
;;
;;   guix package -f guix.scm
;;
;; To build it, but not install it, run:
;;
;;   guix build -f guix.scm
;;
;; To use as the basis for a development environment, run:
;;
;;   guix environment -l guix.scm
;;
;;; Code:

(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix git-download)
  (guix gexp)
  (guix build-system gnu)
  (guix build-system glib-or-gtk)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages gettext)
  (gnu packages glib)
  (gnu packages gnome)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(define %source-dir (dirname (current-filename)))

(package
  (name "gclip_select")
  (version "0.8")
  (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
  (build-system glib-or-gtk-build-system)
  (native-inputs
    (list autoconf
      automake
      pkg-config))
  (inputs (list
  		  guile-3.0
  		  guile-g-golf))
  (synopsis "")
  (description "")
  (home-page "")
  (license license:gpl3+))

