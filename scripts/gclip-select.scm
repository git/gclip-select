#! /bin/sh
# -*- mode: scheme; coding: utf-8 -*-
export export GUILE_LOAD_PATH=`pkg-config g-golf-1.0 --variable=sitedir`:${GUILE_LOAD_PATH}
export GUILE_LOAD_COMPILED_PATH=`pkg-config g-golf-1.0 --variable=siteccachedir`:${GUILE_LOAD_COMPILED_PATH}
export LD_LIBRARY_PATH=`pkg-config g-golf-1.0 --variable=libdir`:${LD_LIBRARY_PATH}
exec guile -e main -s "$0" "$@"
!#

;;;     Copyright 2011, 2013, 2022 Li-Cheng (Andy) Tai
;;;                      atai@atai.org
;;;
;;;     gclip_select is free software: you can redistribute it and/or modify it
;;;     under the terms of the GNU General Public License as published by the Free
;;;     Software Foundation, either version 3 of the License, or (at your option)
;;;     any later version.
;;;
;;;     gclip_select is distributed in the hope that it will be useful, but WITHOUT
;;;     ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;;;     FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;;;     more details.
;;;
;;;     You should have received a copy of the GNU General Public License along with
;;;     gclip_select. If not, see http://www.gnu.org/licenses/.


(use-modules (ice-9 hash-table)
             (ice-9 receive)
             (ice-9 format)
             ((rnrs) :version (6) #:prefix r6rs:)
             (system vm trace)
             )


(define max-num-items 20) ; constant; TO DO: make this configurable from UI

(eval-when (expand load eval)
           (use-modules (oop goops))

           (default-duplicate-binding-handler
             '(merge-generics replace warn-override-core warn last))

           (use-modules (g-golf))
           (use-modules (g-golf glib))
           (g-irepository-require "GLib")
           (for-each (lambda (name)
                       (gi-import-by-name "GLib" name))
                     '("markup_escape_text"
                       "Source"))
           (g-irepository-require "Gdk" #:version "3.0")
           (for-each (lambda (name)
                       (gi-import-by-name "Gdk" name))
                     '("Display"
                       "EventOwnerChange"
                       "Atom"))
           (g-irepository-require "Gtk" #:version "3.0")
           (for-each (lambda (name)
                       (gi-import-by-name "Gtk" name))
                     '("Application"
                       "ApplicationWindow"
                       "Box"
                       "Button"
                       "CellRendererText"
                       "Clipboard"
                       "Label"
                       "ListStore"
                       "ScrolledWindow"
                       "TreeIter"
                       "TreeModel"
                       "TreeView"
                       "TreeViewColumn"
                       "TreeSelection"
                       "main_quit"
                       ))

           (g-irepository-require "Pango")
           (for-each (lambda (name)
                       (gi-import-by-name "Pango" name))
                     '("EllipsizeMode"))
           )

(define (activate-callback app)
  (let* ((clip #f)
         (clip-default #f)
         (content "")
         (content_table (make-hash-table))  ; hash table of content: tree iter
         (content_list '())     ; list of content
         (window (make <gtk-application-window>
                   #:application app
                   #:default-height 200
                   #:default-width 200
                   #:title  "Clipboard Selection Manager"))
         (panel (make <gtk-box>))
         (vbox (make <gtk-box>
                 #:orientation 'vertical
                 #:parent window))
         (list_view (make <gtk-scrolled-window>
                      #:shadow-type  'etched-in))
         (list_box (make <gtk-tree-view>  #:activate-on-single-click #T))
         (list_model (gtk-list-store-new '(string)))
         (delete_button (make <gtk-button> #:label "Delete" #:sensitive #f))
         (delete_all_button (make <gtk-button> #:label "Delete All"  #:sensitive #f)))

    (define (set-clip content)
      (set-text clip content -1)
      (set-text clip-default content -1))

    (define (setup_list_box)
      (let ((text_renderer (make <gtk-cell-renderer-text>
                             #:ellipsize-set #t
                             #:ellipsize 'end
                             #:single-paragraph-mode #t
                             )))
        (set-rules-hint list_box #t)
        (set-model list_box list_model)

        (define column (make <gtk-tree-view-column> #:title "Clip content Selector"))
        (pack-start column text_renderer #f)
        (add-attribute column  text_renderer "text" 0)
        (insert-column list_box column -1)
        (set-headers-visible list_box #f)
        (set-tooltip-column list_box 0)
        (connect list_box 'query-tooltip
                 (lambda (widget x y keyboard_mode tooltip)
                   (let ((selection (get-selection list_box)))
                     (define-values (model iter) (get-selected selection))
                     (when iter
                       (let ((content (gtk-tree-model-get-value model iter 0)))
                         (set-text tooltip (g-markup-escape-text content -1)))
                       #t))))
        (connect list_box 'row-activated
                 (lambda (ignored1 ignored2 ignored3)
                   (let ((content "") (selection (get-selection list_box)))
                     (define-values (model iter) (get-selected selection))
                     (set! content (gtk-tree-model-get-value model iter 0))
                     (set-clip content))))

        (connect list_box 'size-allocate
                 (lambda  rect
                   (let ((vadj (!vadjustment list_view)))
                     (set-value vadj (- (!upper vadj) (!page-size vadj))))))

        (let ((selection (get-selection list_box)))
          (connect selection 'changed
                   (lambda (ignored) (let ((content ""))
                                       (define-values (model iter) (get-selected selection))
                                       (cond (iter
                                              (set-sensitive delete_button #t))
                                             (else
                                              (set-sensitive delete_button #f))))
                           #t))
          #t)))

    (define (remove_item content)
      (let ((iter (hash-ref content_table content)))
        (let  ((f (lambda (key value)
                    (when (eqv? iter value)
                      (hash-remove! content_table key)))))
          (hash-for-each f content_table))
        (set! content_list (delete content content_list))
                                        ;(delete iter content_list)
        (remove list_model iter)
        ))

    (define (check_overflow)
      (let ((num (length content_list))
            (oldest_content #f))
        (when (> num max-num-items)
          (set! oldest_content (list-ref content_list 0)) ; the first item is the oldest
          (remove_item oldest_content))))

    (define (add_entry_to_list_box  content)
      (when (not (= (string-length content) 0))
        (let ((iter (hash-ref content_table content)))
          (when (eqv? iter #f)
            (set! iter (gtk-list-store-insert list_model 0))
            (gtk-list-store-set-value list_model iter 0 content)
            (hash-set! content_table content iter)
            (set! content_list (append content_list (list content)))
            (check_overflow))
          (let ((selection (get-selection list_box)))
                                        ;(assert (is-a? selection <gtk-tree-selection>))
            (gtk-tree-selection-select-iter selection iter)
            (set-sensitive delete_button #t)
            (set-sensitive delete_all_button #t))))
      #t)

    (define (delete_current_selection)
      (let ((content #f)
            (selection (get-selection list_box)))
        (define-values (model iter)
          (gtk-tree-selection-get-selected selection))
        (when iter
          (unselect-iter selection iter)
          (set! content (gtk-tree-model-get-value model iter 0))

          (remove_item content)
          (set-clip "")
          (receive (r iter) (gtk-tree-model-get-iter-first list_model)
            (when (not r)
              (set-sensitive delete_all_button #f)))
          (set! selection (get-selection list_box))
          (receive (model2 iter2)
              (gtk-tree-selection-get-selected selection)

            (when (not iter2)
              (set-sensitive delete_button #f))))))

    (define (delete_all_selection)
      (let ((list_model (get-model list_box))
            (selection (get-selection list_box)))
        (select-all selection)
        (clear list_model)
        (set! content_list '())
        (hash-clear! content_table)
        (set-clip "")
        (set-sensitive delete_button #f)
        (set-sensitive delete_all_button #f)))

    (setup_list_box)
    (add list_view list_box)
    (pack-start vbox list_view #t #t 0)

    (pack-start panel delete_button #t #f 0)
    (pack-start panel delete_all_button #t #f 0)
    (pack-end vbox panel #f #f 0)

    (connect delete_button 'clicked (lambda ignored (delete_current_selection)))
    (connect delete_all_button 'clicked (lambda ignored (delete_all_selection)))
    (set! clip (gtk-clipboard-get-for-display
                (gdk-display-get-default)
                (gdk-atom-intern "PRIMARY" #f)))
    (set! clip-default (gtk-clipboard-get-for-display
                        (gdk-display-get-default)
                        (gdk-atom-intern "CLIPBOARD" #f)))

    (connect window
             'delete-event
             (lambda (window event)
               (gtk-widget-destroy window)
               #f)) ;; do not stop the event propagation
    (connect window 'destroy
             (lambda (object)
               (gtk-main-quit)
               #f))

    (connect clip 'owner-change
             (lambda (_ e)
               (let* ((WAIT_TIME 900); in ms
                      (selection_time (!time e))) ; in ms
                 (g-timeout-add WAIT_TIME
                                (lambda ()
                                  (let*
                                      ((now_time (gettimeofday))
                                       (sec (car now_time))
                                       (nanosec (cdr now_time))
                                       (now (+ (* sec 1000000) (/ nanosec 1000)))) ; in ms

                                    (when (>= (- now selection_time) WAIT_TIME)

                                      (set! content (wait-for-text clip))

                                      (when content
                                        (add_entry_to_list_box content)))
                                    #f))))
               ))

    (set! content (wait-for-text clip))
    (when content
      (add_entry_to_list_box  content))

    (show-all window)))

(define (main args)
  (let ((prog_name "gclip_select")
        (version_string "0.8")
        (app (make <gtk-application>
               #:application-id "org.atai.gclip_select")))
    (format #t "~:a ~:a Copyright 2011, 2013, 2022 Li-Cheng (Andy) Tai ~%" prog_name  version_string)
    (format #t "License: GNU GPL3+ ~%")
    (connect app 'activate activate-callback)
    (let ((status (g-application-run app (length args) args)))
      (exit status))))
